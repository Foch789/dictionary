import java.util.HashMap;
import java.util.List;

import dico.Dictionary;

public class Main {

	public static void main(String[] args) throws Exception 
	{	
		HashMap<String,List<String>> correction;
		double startTime = System.nanoTime();     
		
		
		Dictionary dico = new Dictionary("res/dico.txt");
		dico.cost_correcte = 4;
		dico.number_levenshtein = 5;
		dico.number_trigramme = 100;
		
		double estimatedTimeForDico = (System.nanoTime() - startTime)/1000000000 ;

		//dico.findAllPrint("res/fautes.txt"); //Pour afficher en + les corrections dans la console
		correction = dico.findAll("res/fautes.txt"); 
		
		double estimatedTimeForCorrection = (System.nanoTime() - startTime - estimatedTimeForDico)/1000000000 ;
		
		System.out.println(estimatedTimeForDico + " secondes pour réaliser le dictionnaire.");
		System.out.println(estimatedTimeForCorrection + " secondes pour corriger.");
		
		//System.out.println(correction.toString());	// Pour afficher les corrections
	}

}
