package dico;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;

public class Dictionary {

	public HashSet<String> listDico;
	public HashMap<String,Vector<String>> listDicoTrigrammes;
	public int cost_correcte;
	public int number_levenshtein;
	public int number_trigramme;

	public Dictionary(String fileString) throws Exception
	{
		listDicoTrigrammes = new HashMap<String,Vector<String>>();
		listDico = new HashSet <String>();
		cost_correcte = 3;
		number_levenshtein = 5;
		number_trigramme=100;
		File file = new File(fileString);

		if(!file.exists())
	          throw new Exception("File don't exist");

		if(!file.canRead())
		      throw new Exception("File can't read");


		Scanner scan = new Scanner(file);
		String line;
		Vector<String> tri_line;
		
		System.out.println("Chargement du dictionnaire");
		while(scan.hasNextLine())
		{
			line = scan.nextLine();
			listDico.add(line);
			
			tri_line = new Trigrammes(line).trigrammes;
			
			for(int i = 0; i < tri_line.size();i++)
			{
				if(listDicoTrigrammes.containsKey(tri_line.get(i)))
					listDicoTrigrammes.get(tri_line.get(i)).add(line);
				else
				{
					listDicoTrigrammes.put(tri_line.get(i), new Vector<String>());
					listDicoTrigrammes.get(tri_line.get(i)).add(line);
				}
			}
			
		}
		System.out.println("Fin du chargement");

		scan.close();
	}
	
	public void find(String word)
	{
		HashMap<String,List<String>> returnHashMap = new HashMap<String,List<String>>();
		List<String> test = null;
		
		if(!is_exist(word))
		{
			test = searchTheGoodWord(word);
			returnHashMap.put(word,test);
		}
		else
		{
			test = new LinkedList<String>();
			test.add("le mot est bon");
			returnHashMap.put(word,test);
		}
	}
	
	public HashMap<String,List<String>> findAll(String fileString) throws Exception
	{
		HashMap<String,List<String>> returnHashMap = new HashMap<String,List<String>>();
		List<String> test = null;
		File file = new File(fileString);
		int compteur = 0;
		int compteur_word_false = 0;

		if(!file.exists())
	          throw new Exception("File don't exist");

		if(!file.canRead())
		      throw new Exception("File can't read");
		
		Scanner scan = new Scanner(file);
		String line;
		
		System.out.println("Chargement des fautes");
		while(scan.hasNextLine())
		{
			line = scan.nextLine();
			compteur++;
			if(!is_exist(line))
			{
			    compteur_word_false++;
				test = searchTheGoodWord(line);
				returnHashMap.put(line,test);
				
			}
			else
			{
				test = new LinkedList<String>();
				test.add("le mot est bon");
				returnHashMap.put(line,test);	
			}
		}
		
		scan.close();
		
		System.out.println("Sur "+ compteur + " mots, " + compteur_word_false +" ont été corrigés.");
		
		return returnHashMap;
	}
	
	public void findPrint(String word)
	{
		List<String> test;
		
		if(!is_exist(word))
		{
			System.out.print(word + " (le mot est faux) : ");
			test = searchTheGoodWord(word);
			printList(test);
		}
		else
		{
			System.out.println(word + " : le mot est bon");
		}
	}
	
	public void findAllPrint(String fileString) throws Exception
	{
		List<String> test;
		File file = new File(fileString);
		int compteur = 0;
		int compteur_word_false = 0;

		if(!file.exists())
	          throw new Exception("File don't exist");

		if(!file.canRead())
		      throw new Exception("File can't read");        

		Scanner scan = new Scanner(file);
		String line;
		
		System.out.println("Chargement des fautes");
		while(scan.hasNextLine())
		{
			line = scan.nextLine();
			compteur++;
			if(!is_exist(line))
			{
			    compteur_word_false++;
				System.out.print(line + " (le mot est faux) : ");
				test = searchTheGoodWord(line);
				printList(test);	
			}
			else
			{
				System.out.println(line + " : le mot est bon");
			}
		}
		
		scan.close();
		
		System.out.println("Sur "+ compteur + " mots, " + compteur_word_false +" ont été corrigés.");
	}
	
	private boolean is_exist(String word)
	{
		return listDico.contains(word);
	}
	
	private List<String> searchTheGoodWord(String word)
	{
		
		Vector<String> tri_word = new Trigrammes(word).trigrammes;	
		Map<String,Integer> list_word_possible_no_ordonne = new HashMap<String,Integer>();
		HashSet<String> list_word_possible_no_ordonne_hundred = new HashSet<String>();
		Map<String,Integer> list_word_possible_ordonne = new HashMap<String,Integer>();
		List<String> returnList = new LinkedList<String>();
		
		
		for(int i = 0; i < tri_word.size();i++)
		{
			if(listDicoTrigrammes.containsKey(tri_word.get(i)))
			{
				for(int p = 0; p < listDicoTrigrammes.get(tri_word.get(i)).size();p++)
				{
					if(list_word_possible_no_ordonne.containsKey(listDicoTrigrammes.get(tri_word.get(i)).get(p)))
					{
						list_word_possible_no_ordonne.put(listDicoTrigrammes.get(tri_word.get(i)).get(p), list_word_possible_no_ordonne.get(listDicoTrigrammes.get(tri_word.get(i)).get(p)) + 1);
					}
					else
					{
						list_word_possible_no_ordonne.put(listDicoTrigrammes.get(tri_word.get(i)).get(p), 1 );
					}
				}
			}		
		}
		
		if(list_word_possible_no_ordonne.isEmpty())
		{
			//System.out.print("Aucun mot du Dictionnaire permet de corriger.");
			returnList.add("Aucun mot du Dictionnaire permet de corriger.");
			return returnList;
		}
		
		
		list_word_possible_no_ordonne_hundred = sortByValueSortInverse(list_word_possible_no_ordonne);
		
		Iterator<String> it = list_word_possible_no_ordonne_hundred.iterator();
		String w = new String();
		int cost;
		while(it.hasNext())
		{
			w =  it.next();
			cost = new Levenshtein(word,w).cost;
			if(cost < cost_correcte)
				list_word_possible_ordonne.put(w, cost);
		}
		
		if(list_word_possible_ordonne.isEmpty())
		{
			//System.out.print("Le coût demmander est trop faible.");
			returnList.add("Le coût demmander est trop faible.");
			return returnList;
		}
		
		return returnList = sortByValueSort(list_word_possible_ordonne);
	}
	
	private  List<String> sortByValueSort(Map<String, Integer> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        int n = 1;
        List<String> returnList = new LinkedList<String>();
        for (Map.Entry<String, Integer> entry : list) {
        	returnList.add(entry.getKey());
        	if(n >= number_levenshtein)
        		break;
        	n++;
        }

        return returnList;
    }
	
	private HashSet<String> sortByValueSortInverse(Map<String, Integer> unsortMap) {


		// 1. Convert Map to List of Map
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        
        int n = 1;
        HashSet<String> returnList = new HashSet<String>();
        for (Map.Entry<String, Integer> entry : list) {
        	returnList.add(entry.getKey());
        	if(n >= number_trigramme)
        		break;
        	n++;
        }

        return returnList;
    }
	
    public static <K, V> void printMap(Map<K, V> map) 
    {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            System.out.println("Key : " + entry.getKey()
                    + " Value : " + entry.getValue());
        }
        System.out.println("");
    }
    
    public static void printList(List<String> list) 
    {
    	for(String d : list) {
            System.out.print(d + " | ");
    	}
    	System.out.println("");
    }
    
    public static void printList(List<String> list,String word) 
    {
    	System.out.print(word + " : ");
    	for(String d : list) {
    		System.out.print(d + " | ");
    	}
    	System.out.println("");
    }

}



