package dico;

import java.util.Vector;

public class Trigrammes {
	
	/**
	 * 
	 */
	public Vector<String> trigrammes;
	
	/**
	 * 
	 */
	public int sizeTrigrammes;
	
	/**
	 * 
	 * @param word
	 */
	public Trigrammes(String word) {
		int sizeTrigrammes = word.length();  
		word = "<"+word+">";
		trigrammes = new Vector<String>(); 
		
		for(int i = 1; i <= sizeTrigrammes;i++)
		{
			trigrammes.add(new String(word.charAt(i-1)+""+word.charAt(i)+""+word.charAt(i+1)));
		}
		
	}
	
	

}
