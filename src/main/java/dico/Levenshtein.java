package dico;

public class Levenshtein {
	
	/**
	 * 
	 */
	private String firstWord;
	
	/**
	 * 
	 */
	private String secondWord;
	
	/**
	 * 
	 */
	public int cost;
	
	/**
	 * 
	 * @param firstWord
	 * @param secondWord
	 */
	public Levenshtein(String firstWord,String secondWord)
	{
		this.setFirstWord(firstWord.toLowerCase());
		this.setSecondWord(secondWord.toLowerCase());
		
		Distance();		
	} 
	
	/**
	 * 
	 */
	private void Distance()
	{
		int firstWordSize = this.getFirstWord().length()+1;
		int secondWordSize = this.getSecondWord().length() +1;
		
		int [][] costArray = new int [secondWordSize][firstWordSize];

		// 0 L A U R E N T 8places
		// 0 1 2 3 4 5 6 7 index
		// 1 0 0 0 0 0 0 0
		// 2 0 0 0 0 0 0 0
		// 3 
		// 4
		
		for(int i = 0; i < firstWordSize; i++) costArray[0][i] = i;
		for(int i = 1; i < secondWordSize; i++) costArray[i][0] = i;
		
		for(int i = 1 ; i < secondWordSize; i++)
		{
			for(int j = 1 ; j < firstWordSize; j++)
			{
				costArray[i][j] = searchTheGoodNumber(costArray,i,j);
			}
		}
		
		cost = costArray[this.getSecondWord().length()][this.getFirstWord().length()];
	}
	
	/**
	 * 
	 * @param cost
	 * @param ligne
	 * @param col
	 * @return
	 */
	private int searchTheGoodNumber(int [][] cost,int ligne,int col)
	{
		int minNumber = 0;
		
		if(this.getFirstWord().charAt(col-1) != this.getSecondWord().charAt(ligne-1))
			minNumber = cost[ligne-1][col-1] + 1;
		else
			minNumber = cost[ligne-1][col-1];
		
		minNumber = (cost[ligne-1][col]+1 < minNumber) ? cost[ligne-1][col]+1 : minNumber;
		minNumber = (cost[ligne][col-1]+1  < minNumber) ? cost[ligne][col-1]+1 : minNumber;
		
		return minNumber;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getSecondWord() {
		return secondWord;
	}
	
	
	/**
	 * 
	 * @param secondWord
	 */
	public void setSecondWord(String secondWord) {
		this.secondWord = secondWord;
	}


	/**
	 * 
	 * @return
	 */
	public String getFirstWord() {
		return firstWord;
	}

	/**
	 * 
	 * @param firstWord
	 */
	public void setFirstWord(String firstWord) {
		this.firstWord = firstWord;
	}
	

}
